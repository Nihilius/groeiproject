package be.kdg.f1groeiproject.service;

import be.kdg.f1groeiproject.database.hsql.HSQLUserDAO;
import be.kdg.f1groeiproject.database.UserDAO;
import be.kdg.f1groeiproject.exceptions.UserException;
import be.kdg.f1groeiproject.service.implementation.UserServiceImpl;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class UserServiceTest extends TestCase {
    private UserService userService;
    private UserDAO userDAO;

    class DummyUserDAO implements UserDAO {

        @Override
        public void addUser(User user) {
            //Do nothing
        }

        @Override
        public User getUserByName(String username) {
            return new User(username, "1234567890");
        }
    }

    @Before
    public void setUp() throws Exception {
        userDAO = new HSQLUserDAO();
        userService = new UserServiceImpl(userDAO);
    }

    @Test(expected = UserException.class)
    public void testAddUser() {
        userService.addUser("Ikke", "Nee");
        fail("Te kort password moet exception geven");
    }

    @Test
    public void testLogin() {
        assertFalse(userService.login("NietStefan", "1234567890"));
        assertFalse(userService.login("Stefan", "foutje"));
        assertTrue(userService.login("Stefan", "1234567890"));
        assertTrue(userService.login("sTeFaN", "1234567890"));
    }
}