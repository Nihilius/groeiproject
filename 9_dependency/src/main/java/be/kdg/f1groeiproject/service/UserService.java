package be.kdg.f1groeiproject.service;

public interface UserService {
    void addUser(String username, String password);

    boolean login(String username, String password);
}
