package be.kdg.f1groeiproject.service.implementation;

import be.kdg.f1groeiproject.database.F1TeamsDAO;
import be.kdg.f1groeiproject.service.F1Team;
import be.kdg.f1groeiproject.service.F1TeamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class F1TeamsServiceImpl implements F1TeamsService {
    private static final Logger L = Logger.getLogger(F1TeamsServiceImpl.class.getName());
    private F1TeamsDAO f1TeamsDAO;

    @Autowired
    public F1TeamsServiceImpl(F1TeamsDAO f1TeamsDAO){

        this.f1TeamsDAO = f1TeamsDAO;
    }

    @Override
    public List<F1Team> getAllTeams(){
        L.info("Fetching teamlist from DAO-class");
        List<F1Team> teams = new ArrayList<>();
        teams = f1TeamsDAO.retrieveAll();
        return teams;
    }

    @Override
    public void addF1Team(F1Team f1Team) {
        f1TeamsDAO.addF1Team(f1Team);
    }
}
