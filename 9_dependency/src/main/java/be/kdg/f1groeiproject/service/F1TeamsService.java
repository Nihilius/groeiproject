package be.kdg.f1groeiproject.service;

import java.util.List;

public interface F1TeamsService {
    List<F1Team> getAllTeams();

    void addF1Team(F1Team f1Team);
}
