package be.kdg.f1groeiproject.service.implementation;

import be.kdg.f1groeiproject.database.UserDAO;
import be.kdg.f1groeiproject.exceptions.UserException;
import be.kdg.f1groeiproject.service.User;
import be.kdg.f1groeiproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger L = Logger.getLogger(UserServiceImpl.class.getName());
    private UserDAO userDAO;

    @Autowired
    public UserServiceImpl(UserDAO userDAO){
        L.info("Creating Userservice");
        this.userDAO = userDAO;
    }

    @Override
    public void addUser(String username, String password){
        L.info("Adding user in UserService");
        if(password.length() >= 10 && userDAO.getUserByName(username) == null){
            userDAO.addUser(new User(username, password));
        } else throw new UserException();
    }

    @Override
    public boolean login(String username, String password){
        L.info("Attempting login");
        if (userDAO.getUserByName(username) !=null) {
            if (userDAO.getUserByName(username).getPassword().equals(password)){
                L.info("Login successful!");
                return true;
            }
        }
        L.info("Login failed :(");
        return false;
    }
}
