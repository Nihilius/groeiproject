package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.service.F1TeamsService;
import be.kdg.f1groeiproject.service.UserService;
import be.kdg.f1groeiproject.view.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.logging.Logger;

public class Main_9 extends Application {
    private final static Logger L = Logger.getLogger(Main_9.class.getName());
    public static AnnotationConfigApplicationContext context;
    private LoginView loginView;
    private UserService userService;
    private LoginPresenter loginPresenter;
    private F1TeamsView f1TeamsView;
    private F1TeamsService f1TeamsService;
    private F1TeamsPresenter f1TeamsPresenter;

    public static void main(String[] args) {
        L.info("Starting F1 Team Management System");
        launch(args);
    }

    @Override
    public void init() throws Exception{
        super.init();
        context = new AnnotationConfigApplicationContext();
        context.scan(Main_9.class.getPackageName());
        context.refresh();
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        L.info("Starting the application");
        /*loginView = new LoginView();
        UserDAO userDAO = new HSQLUserDAO();
        userService = new UserServiceImpl(userDAO);
        loginPresenter = new LoginPresenterImpl(loginView, userService);
        Scene scene = new Scene(loginView);*/
        primaryStage.setScene(context.getBean(Scene.class));
        primaryStage.show();

        /*f1TeamsView = new F1TeamsView();
        f1TeamsService = new F1TeamsService();
        f1TeamsPresenter = new F1TeamsPresenter(f1TeamsView, f1TeamsService);
        Scene scene = new Scene(f1TeamsView);
        primaryStage.setScene(scene);
        primaryStage.show();*/
    }

    @Override
    public void stop() throws Exception {
        context.stop();
        super.stop();
    }
}
