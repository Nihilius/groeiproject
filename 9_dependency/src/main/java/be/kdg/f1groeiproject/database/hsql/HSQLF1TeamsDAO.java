package be.kdg.f1groeiproject.database.hsql;

import be.kdg.f1groeiproject.database.DataSource;
import be.kdg.f1groeiproject.database.F1TeamsDAO;
import be.kdg.f1groeiproject.exceptions.F1TeamException;
import be.kdg.f1groeiproject.service.F1Team;
import be.kdg.f1groeiproject.service.Hoofdkwartier;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class HSQLF1TeamsDAO implements F1TeamsDAO {
    private static final Logger L = Logger.getLogger(HSQLF1TeamsDAO.class.getName());
    private DataSource dataSource = DataSource.getInstance();

    @Override
    public List<F1Team> retrieveAll() {
        try {
            List<F1Team> teams = new ArrayList<>();
            Statement statement = dataSource.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM F1TEAMS");
            while (resultSet.next()) {
                teams.add(new F1Team(resultSet.getString("TEAMNAME"),
                        resultSet.getString("COUREUR"),
                        resultSet.getDouble("WINPERCENT"),
                        resultSet.getDate("EERSTEDEELNAME").toLocalDate(),
                        resultSet.getInt("KAMPIOENSCHAPPEN"),
                        Hoofdkwartier.valueOf(resultSet.getString("HOOFDKWARTIER"))));
            }
            return teams;
        } catch (SQLException e){
            L.warning("Problem retrieving teamlist from DB");
            throw new F1TeamException("Problem retrieving teamlist from DB", e);
        }
    }

    @Override
    public void addF1Team(F1Team f1Team) {
        try {
            PreparedStatement statement = dataSource.getConnection().prepareStatement("INSERT INTO F1TEAMS " +
                    "(TEAMNAME, COUREUR, WINPERCENT, EERSTEDEELNAME, KAMPIOENSCHAPPEN, HOOFDKWARTIER) " +
                    "VALUES (?, ?, ?, ?, ?, ?)");
            statement.setString(1, f1Team.getTeamNaam());
            statement.setString(2, f1Team.getMeestSuccesCoureur());
            statement.setDouble(3, f1Team.getWinPercent());
            statement.setDate(4, Date.valueOf(f1Team.getEersteDeelname()));
            statement.setInt(5, f1Team.getKampioenschappen());
            statement.setString(6, f1Team.getHoofdkwartier().name());
            statement.executeUpdate();
        } catch (SQLException e){
            L.warning("Probleem met toevoegen F1 Team");
            throw new F1TeamException("Probleem met toevoegen F1 Team", e);
        }
    }
}
