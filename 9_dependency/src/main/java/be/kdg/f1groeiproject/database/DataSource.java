package be.kdg.f1groeiproject.database;

import be.kdg.f1groeiproject.exceptions.UserException;

import javax.xml.crypto.Data;
import java.sql.*;
import java.util.logging.Logger;

public final class DataSource {
    private static final Logger L = Logger.getLogger(DataSource.class.getName());
    private Connection connection;
    private static DataSource enigeDataSource = null;

    private DataSource() {
        try {
            L.info("Creating DataSource");
            connection = DriverManager.getConnection("jdbc:hsqldb:file:database/f1teamsServicedb", "sa", "");
            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "USERS", null);
            if (!tables.next()) {
                L.info("Creating table: USERS");
                Statement statement = connection.createStatement();
                statement.execute("CREATE TABLE USERS (ID INTEGER IDENTITY  NOT NULL, NAME VARCHAR(100) NOT NULL, PASSWORD VARCHAR(20) NOT NULL )");
                statement.close();
            }
            ResultSet table = dbm.getTables(null, null, "F1TEAMS", null);
            if (!table.next()) {
                Statement statement = connection.createStatement();
                statement.execute("CREATE TABLE F1TEAMS " +
                        "(ID INTEGER IDENTITY NOT NULL , " +
                        "TEAMNAME VARCHAR(100) NOT NULL, " +
                        "COUREUR VARCHAR(100) NOT NULL," +
                        "WINPERCENT DOUBLE," +
                        "EERSTEDEELNAME DATE NOT NULL," +
                        "KAMPIOENSCHAPPEN INTEGER NOT NULL," +
                        "HOOFDKWARTIER VARCHAR(10) NOT NULL)");
                statement.close();
            }
        } catch (SQLException e) {
            L.warning("Problem creating Datasource");
            throw new UserException("Problem creating DataSource", e);
        }
    }

    public Connection getConnection() {
        L.info("Accessing Connection");
        return connection;
    }

    public void closeConnection() {
        try {
            L.info("Closing connection");
            connection.close();
        } catch (SQLException e) {
            L.warning("Problem closing connection");
            throw new UserException("Problem closing connection", e);
        }
    }

    public static synchronized DataSource getInstance() {
        L.info("DataSource object wordt aangevraagd");
        if (enigeDataSource == null) {
            enigeDataSource = new DataSource();
        }
        return enigeDataSource;
    }
}
