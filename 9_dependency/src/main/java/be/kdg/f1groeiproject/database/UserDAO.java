package be.kdg.f1groeiproject.database;

import be.kdg.f1groeiproject.service.User;

public interface UserDAO {
    void addUser(User user);

    User getUserByName(String username);
}
