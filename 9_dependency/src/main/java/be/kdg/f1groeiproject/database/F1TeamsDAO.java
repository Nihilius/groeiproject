package be.kdg.f1groeiproject.database;

import be.kdg.f1groeiproject.service.F1Team;

import java.util.List;

public interface F1TeamsDAO {
    List<F1Team> retrieveAll();

    void addF1Team(F1Team f1Team);
}
