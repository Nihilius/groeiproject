package be.kdg.f1groeiproject.database.hsql;

import be.kdg.f1groeiproject.database.DataSource;
import be.kdg.f1groeiproject.database.UserDAO;
import be.kdg.f1groeiproject.exceptions.UserException;
import be.kdg.f1groeiproject.service.User;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

@Repository
public class HSQLUserDAO implements UserDAO {
    private final static Logger L = Logger.getLogger(HSQLUserDAO.class.getName());
    private final DataSource dataSource = DataSource.getInstance();

    @Override
    public void addUser(User user) {
        try {
            L.info("Adding user");
            PreparedStatement prepStatement = dataSource.getConnection().prepareStatement("INSERT INTO USERS (NAME, PASSWORD) VALUES (?, ?)");
            prepStatement.setString(1, user.getName());
            prepStatement.setString(2, user.getPassword());
            prepStatement.execute();
            prepStatement.close();
        } catch (SQLException e){
            L.warning("Problem with adding user");
            throw new UserException("Problem in user creation", e);
        }
    }

    @Override
    public User getUserByName(String username) {
        try {
            L.info("Looking for user");
            PreparedStatement statement = dataSource.getConnection().prepareStatement("SELECT * FROM USERS WHERE NAME=?");
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                return new User(resultSet.getString("NAME"), resultSet.getString("PASSWORD"));
            }
            return null;
        } catch (SQLException e) {
            L.warning("Problem retrieving user");
            throw new UserException("Problem retrieving user", e);
        }
    }
}
