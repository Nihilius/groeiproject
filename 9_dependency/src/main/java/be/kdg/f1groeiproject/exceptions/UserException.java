package be.kdg.f1groeiproject.exceptions;

public class UserException extends RuntimeException {
    public UserException() {
    }

    public UserException(String message, Throwable cause) {
        super(message, cause);
    }
}
