package be.kdg.f1groeiproject.exceptions;

public class F1TeamException extends RuntimeException {
    public F1TeamException() {
    }

    public F1TeamException(String message, Throwable cause) {
        super(message, cause);
    }
}
