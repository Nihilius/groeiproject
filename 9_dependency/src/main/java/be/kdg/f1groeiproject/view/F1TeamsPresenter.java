package be.kdg.f1groeiproject.view;

public interface F1TeamsPresenter {
    void loadF1Teams();

    void addHandlers();
}
