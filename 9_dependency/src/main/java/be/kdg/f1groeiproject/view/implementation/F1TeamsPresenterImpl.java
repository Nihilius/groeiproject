package be.kdg.f1groeiproject.view.implementation;

import be.kdg.f1groeiproject.service.Hoofdkwartier;
import be.kdg.f1groeiproject.service.F1Team;
import be.kdg.f1groeiproject.service.F1TeamsService;
import be.kdg.f1groeiproject.view.F1TeamsPresenter;
import be.kdg.f1groeiproject.view.F1TeamsView;
import javafx.collections.FXCollections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class F1TeamsPresenterImpl implements F1TeamsPresenter {
    private final static Logger L = Logger.getLogger(F1TeamsPresenterImpl.class.getName());
    private F1TeamsView f1TeamsView;
    private F1TeamsService f1TeamsService;

    @Autowired
    public F1TeamsPresenterImpl(F1TeamsView f1TeamsView, F1TeamsService f1TeamsService) {
        L.info("Creating F1TeamsPresenter");
        this.f1TeamsView = f1TeamsView;
        this.f1TeamsService = f1TeamsService;
        loadF1Teams();
        addHandlers();
    }

    @Override
    public void loadF1Teams() {
        List<F1Team> myList = new ArrayList<>(f1TeamsService.getAllTeams());
        myList.add(new F1Team("Ferrari", "Schumacher", 0.25, LocalDate.of(1950, 4, 25), 16, Hoofdkwartier.ITALY));
        myList.add(new F1Team("Mercedes", "Hamilton", 0.45, LocalDate.of(1955, 4, 25), 8, Hoofdkwartier.UK));
        myList.add(new F1Team("Red Bull", "Vettel", 0.18, LocalDate.of(2006, 4, 25), 4, Hoofdkwartier.UK));
        f1TeamsView.getTvF1Teams().setItems(FXCollections.observableList(myList));
    }

    @Override
    public void addHandlers(){
        f1TeamsView.getBtnSave().setOnAction(event -> {
            f1TeamsService.addF1Team(new F1Team(f1TeamsView.getTfName().getText(),
                    f1TeamsView.getTfDriver().getText(),
                    Double.parseDouble(f1TeamsView.getTfWinPercent().getText()),
                    f1TeamsView.getDpEersteDeelname().getValue(),
                    Integer.parseInt(f1TeamsView.getTfKampioenschappen().getText()),
                    Hoofdkwartier.valueOf(f1TeamsView.getCbLocatie().getValue().toString())));
        });
        loadF1Teams();
    }
}
