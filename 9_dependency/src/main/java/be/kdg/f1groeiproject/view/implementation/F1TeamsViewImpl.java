package be.kdg.f1groeiproject.view.implementation;

import be.kdg.f1groeiproject.service.Hoofdkwartier;
import be.kdg.f1groeiproject.service.F1Team;
import be.kdg.f1groeiproject.view.F1TeamsView;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class F1TeamsViewImpl extends BorderPane implements F1TeamsView {
    private TableView tvF1Teams;
    private TextField tfName;
    private TextField tfDriver;
    private DatePicker dpEersteDeelname;
    private TextField tfWinPercent;
    private TextField tfKampioenschappen;
    private ChoiceBox cbLocatie;
    private Button btnSave;
    private HBox hBox;

    public F1TeamsViewImpl() {
        tvF1Teams = new TableView<>();
        this.setCenter(tvF1Teams);
        TableColumn<String, F1Team> column1 = new TableColumn<>("Teamname");
        column1.setCellValueFactory(new PropertyValueFactory<>("teamNaam"));
        TableColumn<String, F1Team> column2 = new TableColumn<>("Most Successsfull Driver");
        column2.setCellValueFactory(new PropertyValueFactory<>("meestSuccesCoureur"));
        TableColumn<Double, F1Team> column3 = new TableColumn<>("Win Percentage");
        column3.setCellValueFactory(new PropertyValueFactory<>("winPercent"));
        TableColumn<LocalDate, F1Team> column4 = new TableColumn<>("First Race");
        column4.setCellValueFactory(new PropertyValueFactory<>("eersteDeelname"));
        TableColumn<Integer, F1Team> column5 = new TableColumn<>("# Constructor Championships");
        column5.setCellValueFactory(new PropertyValueFactory<>("kampioenschappen"));
        TableColumn<Hoofdkwartier, F1Team> column6 = new TableColumn<>("HQ");
        column6.setCellValueFactory(new PropertyValueFactory<>("hoofdkwartier"));
        tvF1Teams.getColumns().addAll(column1, column2, column3, column4, column5, column6);
        tfName = new TextField();
        tfName.setPromptText("Teamname");
        tfDriver = new TextField();
        tfDriver.setPromptText("Drivername");
        dpEersteDeelname = new DatePicker();
        tfWinPercent = new TextField();
        tfWinPercent.setPromptText("Win Percentage");
        tfKampioenschappen = new TextField();
        tfKampioenschappen.setPromptText("Championships");
        cbLocatie = new ChoiceBox();
        cbLocatie.setItems(FXCollections.observableArrayList(Hoofdkwartier.ITALY.name(), Hoofdkwartier.UK.name(), Hoofdkwartier.US.name()));
        btnSave = new Button("Save");
        hBox = new HBox();
        hBox.getChildren().addAll(tfName, tfDriver, dpEersteDeelname, tfWinPercent, tfKampioenschappen, cbLocatie, btnSave);
        this.setBottom(hBox);
    }

    @Override public TableView getTvF1Teams() {
        return tvF1Teams;
    }

    @Override public TextField getTfName() {
        return tfName;
    }

    @Override public TextField getTfDriver() {
        return tfDriver;
    }

    @Override public DatePicker getDpEersteDeelname() {
        return dpEersteDeelname;
    }

    @Override public TextField getTfWinPercent() {
        return tfWinPercent;
    }

    @Override public TextField getTfKampioenschappen() {
        return tfKampioenschappen;
    }

    @Override public ChoiceBox getCbLocatie() {
        return cbLocatie;
    }

    @Override public Button getBtnSave() {
        return btnSave;
    }
}


