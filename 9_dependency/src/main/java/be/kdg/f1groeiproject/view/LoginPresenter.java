package be.kdg.f1groeiproject.view;

import javafx.scene.Scene;

public interface LoginPresenter {
    void addEventHandlers(Scene scene, F1TeamsView f1TeamsView);
}
