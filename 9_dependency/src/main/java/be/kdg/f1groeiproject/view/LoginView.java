package be.kdg.f1groeiproject.view;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public interface LoginView {
    TextField getUserText();

    TextField getPassText();

    Button getLoginBtn();

    Button getAddBtn();
}
