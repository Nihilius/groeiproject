package be.kdg.f1groeiproject.view;

import javafx.scene.Parent;
import javafx.scene.Scene;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DIConfiguration {
    @Bean
    public Scene scene(LoginView loginView){
        return new Scene((Parent) loginView);
    }
}
