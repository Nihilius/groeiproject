package be.kdg.f1groeiproject.view;

import javafx.scene.control.*;

public interface F1TeamsView {
    TableView getTvF1Teams();

    TextField getTfName();

    TextField getTfDriver();

    DatePicker getDpEersteDeelname();

    TextField getTfWinPercent();

    TextField getTfKampioenschappen();

    ChoiceBox getCbLocatie();

    Button getBtnSave();
}
