package be.kdg.f1groeiproject.view.implementation;

import be.kdg.f1groeiproject.service.UserService;
import be.kdg.f1groeiproject.view.F1TeamsView;
import be.kdg.f1groeiproject.view.LoginPresenter;
import be.kdg.f1groeiproject.view.LoginView;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class LoginPresenterImpl implements LoginPresenter {
    private final static Logger L = Logger.getLogger(LoginPresenterImpl.class.getName());
    private LoginView loginView;
    private UserService userService;

    @Autowired
    public LoginPresenterImpl(LoginView loginView, UserService userService, Scene scene, F1TeamsView f1TeamsView) {
        L.info("Construction LoginPresenter");
        this.loginView = loginView;
        this.userService = userService;
        addEventHandlers(scene, f1TeamsView);
    }

    @Override
    public void addEventHandlers(Scene scene, F1TeamsView f1TeamsView) {
        L.info("Adding eventhandlers");
        loginView.getLoginBtn().setOnAction(event -> {
            if (loginView.getUserText().getText() != null && loginView.getPassText().getText() != null) {
                userService.login(loginView.getUserText().getText(), loginView.getPassText().getText());
                L.info("Login succeeded!");
                scene.setRoot((Parent) f1TeamsView);
                scene.getWindow().sizeToScene();
            } else event.consume();
        });
        loginView.getAddBtn().setOnAction(event -> {
            if (loginView.getUserText().getText() != null && loginView.getPassText().getText() != null) {
                userService.addUser(loginView.getUserText().getText(), loginView.getPassText().getText());
            } else event.consume();
        });
    }
}
