package be.kdg.f1groeiproject.view.implementation;

import be.kdg.f1groeiproject.view.LoginView;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class LoginViewImpl extends BorderPane implements LoginView {
    private final static Logger L = Logger.getLogger(LoginView.class.getName());
    private final GridPane grid;
    private final Label user;
    private final TextField userText;
    private final Label pass;
    private final TextField passText;
    private final HBox hbox;
    private final Button loginBtn;
    private final Button addBtn;

    public LoginViewImpl() {
        L.info("Constructing LoginView!");
        grid = new GridPane();
        user = new Label("User:");
        userText = new TextField();
        grid.add(user, 0, 0);
        grid.add(userText, 1, 0);
        pass = new Label("Password:");
        passText = new PasswordField();
        grid.add(pass, 0, 1);
        grid.add(passText, 1, 1);
        this.setCenter(grid);
        hbox = new HBox();
        loginBtn = new Button("Login");
        addBtn = new Button("Add");
        hbox.getChildren().addAll(loginBtn, addBtn);
        this.setBottom(hbox);
    }

    @Override public TextField getUserText() {
        return userText;
    }

    @Override public TextField getPassText() {
        return passText;
    }

    @Override public Button getLoginBtn() {
        return loginBtn;
    }

    @Override public Button getAddBtn() {
        return addBtn;
    }
}
