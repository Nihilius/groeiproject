package be.kdg.f1groeiproject.logging;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SmallLogFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        return Instant.ofEpochMilli(record.getMillis()).atZone(ZoneId.systemDefault()).toLocalDate()
                + " Level: "
                + record.getLevel()
                + " melding: "
                + MessageFormat.format(record.getMessage(), record.getParameters())
                + "\n";
    }
}
