package be.kdg.f1groeiproject.model;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class F1Teams {
    private Set<F1Team> f1teams = new TreeSet<>();
    private static final Logger logger = Logger.getLogger(F1Teams.class.getName());

    public boolean voegToe(F1Team team) {
        boolean nieuw = true;

        for (F1Team f1Team : getF1teams()) {
            if (f1Team.equals(team) && f1Team.hashCode() == team.hashCode()) {
                nieuw = false;
                break;
            }
        }
        if (nieuw) {
            getF1teams().add(team);
            logger.finer(team.getTeamNaam() + " werd toegevoegd aan de set");
            return nieuw;
        }
        return nieuw;
    }

    public boolean verwijder(String teamnaam) {
        Iterator iterator = getF1teams().iterator();
        while (iterator.hasNext()) {
            Object team = iterator.next();
            if (((F1Team) team).getTeamNaam().equals(teamnaam)) {
                iterator.remove();
                logger.log(Level.FINER, teamnaam + " werd verwijderd uit de set");
                return true;
            }
        }
        return false;
    }

    public F1Team zoek(String teamnaam) {
        for (F1Team f1team : getF1teams()) {
            if (f1team.getTeamNaam().equals(teamnaam)) {
                return f1team;
            }
        }
        return null;
    }


    public List<F1Team> gesorteerdOpNaam() {
        List<F1Team> lijstOpNaam = new ArrayList<>(getF1teams());
        return lijstOpNaam;

    }

    public List<F1Team> gesorteerdOpEersteDeelname() {
        class SorteerOpDeelname implements Comparator<F1Team> {


            @Override
            public int compare(F1Team o1, F1Team o2) {
                return o1.getEersteDeelname().compareTo(o2.getEersteDeelname());
            }
        }
        List<F1Team> sorteerOpDeelName = new ArrayList<>(getF1teams());

        Collections.sort(sorteerOpDeelName, new SorteerOpDeelname());

        return sorteerOpDeelName;

    }

    public List<F1Team> gesorteerdOpKampioenschappen() {
        class SorteerOpKampioen implements Comparator<F1Team> {

            @Override
            public int compare(F1Team o1, F1Team o2) {
                return o1.getKampioenschappen().compareTo(o2.getKampioenschappen());
            }
        }
        List<F1Team> sorteerOpKampioenschappen = new ArrayList<>(getF1teams());
        Collections.sort(sorteerOpKampioenschappen, new SorteerOpKampioen());
        Collections.reverse(sorteerOpKampioenschappen);
        return sorteerOpKampioenschappen;
    }

    public int getAantal() {
        return getF1teams().size();
    }

    public Set<F1Team> getF1teams() {
        return f1teams;
    }

    public void setF1teams(Set<F1Team> f1teams) {
        this.f1teams = f1teams;
    }
}
