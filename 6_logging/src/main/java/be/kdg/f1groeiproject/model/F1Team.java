package be.kdg.f1groeiproject.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class F1Team implements Comparable {
    private String teamNaam;
    private String meestSuccesCoureur;
    private Double winPercent;
    private LocalDate eersteDeelname;
    private Integer kampioenschappen;
    private Hoofdkwartier hoofdkwartier;
    private static final Logger logger = Logger.getLogger(F1Team.class.getName());

    //Constructors

    public F1Team() {
        new F1Team("Team", "John Doe", 0.0, LocalDate.now(), 0, Hoofdkwartier.UK);
    }

    public F1Team(String teamNaam, String meestSuccesCoureur, Double winPercent, LocalDate eersteDeelname, Integer kampioenschappen, Hoofdkwartier hoofdkwartier) {
        setTeamNaam(teamNaam);
        setMeestSuccesCoureur(meestSuccesCoureur);
        setWinPercent(winPercent);
        setEersteDeelname(eersteDeelname);
        setKampioenschappen(kampioenschappen);
        setHoofdkwartier(hoofdkwartier);
    }


    //<editor-fold desc="Setters">
    public void setTeamNaam(String teamNaam) {
        if (!(teamNaam.length() == 0)) {
            this.teamNaam = teamNaam;
        } else {
            logger.severe(teamNaam + " is geen geldige teamnaam");
        }
    }

    public void setMeestSuccesCoureur(String meestSuccesCoureur) {
        if (!(meestSuccesCoureur.length() == 0)) {
            this.meestSuccesCoureur = meestSuccesCoureur;
        } else {
            logger.severe(meestSuccesCoureur + " is geen geldige coureursnaam voor "+ getTeamNaam());
        }
    }

    public void setWinPercent(Double winPercent) {
        if (winPercent >= 0 && winPercent <= 1) {
            this.winPercent = winPercent;
        } else {
            logger.severe(winPercent + " is geen geldig winpercentage voor " + getTeamNaam());
        }
    }

    public void setEersteDeelname(LocalDate eersteDeelname) {
        if (eersteDeelname.compareTo(LocalDate.now()) <= 0) {
            this.eersteDeelname = eersteDeelname;
        } else {
            logger.severe("Eerste deelname " + eersteDeelname + " ligt niet in het verleden voor " + getTeamNaam());
        }
    }

    public void setKampioenschappen(Integer kampioenschap) {
        if (this.eersteDeelname != null) {
            if (kampioenschap >= 0 && LocalDate.now().getYear() - this.eersteDeelname.getYear() >= kampioenschap) {
                this.kampioenschappen = kampioenschap;
            } else {
                logger.severe("Het aantal kampioenschappen: " + kampioenschap + ". Is lager dan nul of hoger dan het aantal deelgenomen seizonen voor " + getTeamNaam());
            }
        }
    }

    public void setHoofdkwartier(Hoofdkwartier hoofdkwartier) {
        this.hoofdkwartier = hoofdkwartier;
    }
    //</editor-fold>

    //<editor-fold desc="Getters">

    public String getTeamNaam() {
        return teamNaam;
    }

    public String getMeestSuccesCoureur() {
        return meestSuccesCoureur;
    }

    public Double getWinPercent() {
        return winPercent;
    }

    public LocalDate getEersteDeelname() {
        return eersteDeelname;
    }

    public Integer getKampioenschappen() {
        return kampioenschappen;
    }

    public Hoofdkwartier getHoofdkwartier() {
        return hoofdkwartier;
    }
    //</editor-fold>


//Functies

    @Override
    public int hashCode() {
        return Objects.hash(this.teamNaam);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        F1Team f1Team = (F1Team) obj;
        return Objects.equals(this.teamNaam, f1Team.teamNaam);
    }

    @Override
    public String toString() {
        return String.format("%30s%44s%s%34s%s%40s%d\n", teamNaam, "Succesvolste coureur: ", meestSuccesCoureur, "Eerste Deelname: ", eersteDeelname.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), "Aantal Kampioenschappen: ", kampioenschappen);
    }


    @Override
    public int compareTo(Object o) {
        return this.teamNaam.compareTo(((F1Team) o).teamNaam);
    }
}
