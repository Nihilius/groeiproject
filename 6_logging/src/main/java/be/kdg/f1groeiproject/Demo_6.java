package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import be.kdg.f1groeiproject.model.Hoofdkwartier;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.LogManager;
import java.util.logging.FileHandler;

public class Demo_6 {
    public static void main(String[] args) {
        URL configURL = Demo_6.class.getResource("/logging.properties");
        if (configURL != null){
            try (InputStream is = configURL.openStream()){
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.out.println("Configuratiebestand corrupt");
            }
        } else System.out.println("Configuratiebestand niet gevonden");

        F1Teams f1Teams = new F1Teams();
        new F1Team("", "Schumacher", 0.24, LocalDate.of(1950, 6, 13), 16, Hoofdkwartier.ITALY);
        new F1Team("Scuderia Ferrari", "Schumacher", 0.24, LocalDate.of(1950, 6, 13), 16, Hoofdkwartier.ITALY);
        new F1Team("Scuderia Ferrari", "", 0.24, LocalDate.of(1950, 6, 13), 16, Hoofdkwartier.ITALY);
        new F1Team("Scuderia Ferrari", "Schumacher", 6.0, LocalDate.of(1950, 6, 13), 16, Hoofdkwartier.ITALY);
        new F1Team("Scuderia Ferrari", "Schumacher", 0.24, LocalDate.of(2021, 6, 13), 16, Hoofdkwartier.ITALY);
        new F1Team("Scuderia Ferrari", "Schumacher", 0.24, LocalDate.of(2019, 6, 13), 16, Hoofdkwartier.ITALY);

        f1Teams.voegToe(new F1Team("Scuderia Ferrari", "Schumacher", 0.24, LocalDate.of(1950, 6, 13), 16, Hoofdkwartier.ITALY));
        f1Teams.voegToe(new F1Team("Team Lotus", "Jim Clark", 0.15133,  LocalDate.of(1958,5,18), 7, Hoofdkwartier.UK));
        f1Teams.verwijder("Scuderia Ferrari");
    }
}
