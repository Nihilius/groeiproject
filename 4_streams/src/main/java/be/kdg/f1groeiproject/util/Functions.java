package be.kdg.f1groeiproject.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class Functions {
    public static <T> List<T> filteredList(List<T> teamList, Predicate<T> predicate) {
        return teamList.stream().filter(t -> predicate.test(t)).collect(Collectors.toList());
    }

    public static <T> Double average(List<T> teamList, ToDoubleFunction<T> mapper) {
       return teamList.stream().mapToDouble(mapper).average().getAsDouble();
    }

    public static <T> long countIf(List<T> teamList, Predicate<T> predicate) {
        return teamList.stream().filter(t -> predicate.test(t)).count();
    }
}
