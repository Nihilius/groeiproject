package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.data.Data;
import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import be.kdg.f1groeiproject.model.Hoofdkwartier;
import be.kdg.f1groeiproject.util.Functions;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Demo_4 {
    public static void main(String[] args) {
        F1Teams teams = new F1Teams();
        List<F1Team> data = new Data().getData();

        data.forEach(f1Team -> teams.voegToe(f1Team));

        System.out.println("\nF1 teams gesoprteerd op naam: ");
        teams.sortedBy(F1Team::getTeamNaam).forEach(f1Team -> System.out.println(f1Team));

        System.out.println("\nF1 teams gesoprteerd op eerste deelname: ");
        teams.sortedBy(F1Team::getEersteDeelname).forEach(f1Team -> System.out.println(f1Team));

        System.out.println("\nF1 teams gesoprteerd op kampioenschappen: ");
        teams.sortedBy(F1Team::getKampioenschappen).forEach(f1Team -> System.out.println(f1Team));

        System.out.println("\nF1 teams gefiltered op meer dan 4 kampioenschappen: ");
        Functions.filteredList(data, team -> team.getKampioenschappen() > 4).forEach(f1Team -> System.out.println(f1Team));

        System.out.println("\nF1 teams gefiltered op eerste deelname voor 1960: ");
        Functions.filteredList(data, team -> team.getEersteDeelname().isBefore(LocalDate.parse("1960-01-01"))).forEach(f1Team -> System.out.println(f1Team));

        System.out.println("\nF1 teams gefiltered op teams die minstens een kwart van hun races wonnen: ");
        Functions.filteredList(data, team -> team.getWinPercent() >= 0.25).forEach(f1Team -> System.out.println(f1Team));

        System.out.printf("Gemiddeld aantal kampioenschappen: %.1f \n",
                Functions.average(data, F1Team::getKampioenschappen));

        System.out.printf("Aantal teams met hoofkwartier in het VK: %d\n"
                , Functions.countIf(data, team -> team.getHoofdkwartier() == Hoofdkwartier.UK));


        /*
        Streaming
         */
        System.out.println("\nStream opdrachten \n");
        System.out.println("Het aantal teams dat minstens 1 kampioenschap heeft gewonnen: "
                + data.stream().filter(f1Team -> f1Team.getKampioenschappen() > 0).count());
        System.out.print("Gestorteerd op winpercentage en erna gesorteerd op kampioenschappen: \n");
                data.stream().
                        sorted(Comparator.comparingDouble(F1Team::getWinPercent).reversed()).
                        sorted(Comparator.comparingInt(F1Team::getKampioenschappen).reversed()).
                        forEach(f1Team -> System.out.print(f1Team.toString()));
        System.out.print("\nAlle Succesvolste coureur in hoofdletters, omgekeerd gesorteerd en zonder dubbels: ");
        data.stream().sorted(Comparator.comparing(F1Team::getMeestSuccesCoureur).reversed())
                .map(f1Team -> f1Team.getMeestSuccesCoureur()).distinct()
                .forEach(f1team -> System.out.print(f1team.toUpperCase()+ ", "));

        System.out.println("\nEen willekeurig team met meer dan 10% overwinningen: ");
        System.out.println(data.stream().filter(f1Team -> f1Team.getWinPercent()>0.10).findAny().get());

        System.out.println("Het team met het hoogste winpercentage: ");
        System.out.println(data.stream().max(Comparator.comparing(F1Team::getWinPercent)).get());

        System.out.println("Lijst van teams met het woord \"team\" in hun naam: ");
        System.out.println(data.stream().filter(f1Team -> f1Team.getTeamNaam().toLowerCase().contains("team"))
                .collect(Collectors.toList()));
        System.out.println("Teams verdeeld over eerste deelname voor/na 1965: ");
        Map<Boolean, List<F1Team>> map = data.stream()
                .collect(Collectors.partitioningBy(f1Team -> f1Team.getEersteDeelname()
                        .isBefore(LocalDate.parse("1965-01-01"))));
        System.out.println("Team before 1965: ");
        map.get(true).forEach(f1Team -> System.out.println(f1Team.toString()));
        System.out.println("Team after 1965: ");
        map.get(false).forEach(f1Team -> System.out.println(f1Team.toString()));
    }
}
