package be.kdg.f1groeiproject.reflection;



import be.kdg.f1groeiproject.CanRun;

import java.lang.reflect.*;


public class ReflectionTools {
    public static void classAnyalis(Class... aClass) {
        try {
            for (int i = 0; i < aClass.length; i++) {
                Object object = aClass[i].getConstructor().newInstance();
                System.out.println("Analyse van de klasse: " + object.getClass().getSimpleName());
                System.out.println("====================================================================");
                System.out.println("Fully qualified name: " + object.getClass().getName());
                System.out.println("Naam van de superklasse: " + object.getClass().getSuperclass().getName());
                System.out.println("Naam van de Package: " + object.getClass().getPackage().getName());
                System.out.print("Interfaces: ");
                for (Class inter : object.getClass().getInterfaces()) {
                    System.out.print(" " + inter.getSimpleName());
                }
                System.out.println();
                System.out.print("Constructors: ");
                for (Constructor constructor : object.getClass().getConstructors()) {
                    System.out.print(" " + constructor.toGenericString());
                }
                System.out.println();
                System.out.print("Attributen: ");
                for (Field field : object.getClass().getDeclaredFields()) {
                    System.out.print(" " + field.getName());
                }
                System.out.println();
                System.out.print("Getters: ");
                for (Method method : object.getClass().getDeclaredMethods()) {
                    if (method.getName().startsWith("get")) {
                        System.out.print(" " + method.getName());
                    }
                }
                System.out.println();
                System.out.print("Setters: ");
                for (Method method : object.getClass().getDeclaredMethods()) {
                    if (method.getName().startsWith("set")) {
                        System.out.print(" " + method.getName());
                    }
                }
                System.out.println();
                System.out.print("Andere methoden: ");
                for (Method method : object.getClass().getDeclaredMethods()) {
                    if (!method.getName().startsWith("set") && !method.getName().startsWith("get")) {
                        System.out.print(" " + method.getName());
                    }
                }
                System.out.println();
            }
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static Object runAnnotated(Class aClass) {
        Object object =null;
        try {
            object = aClass.getConstructor().newInstance();
            for (Method method : object.getClass().getMethods()) {
                if (method.getAnnotations() != null) {
                    for (Type parameter : method.getGenericParameterTypes()) {
                        if (parameter == String.class) {
                            String value = method.getAnnotation(CanRun.class).value();
                            method.invoke(object, value);
                        }
                    }

                }
            }

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
        return object;
    }
}

