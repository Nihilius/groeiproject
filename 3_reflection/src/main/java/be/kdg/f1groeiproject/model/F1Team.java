package be.kdg.f1groeiproject.model;

import be.kdg.f1groeiproject.CanRun;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class F1Team extends Team {
    private String meestSuccesCoureur;
    private Double winPercent;
    private Integer kampioenschappen;

    //Constructors

    public F1Team() {
        super();
        setMeestSuccesCoureur("John Doe");
        setWinPercent(0.0);
        setKampioenschappen(0);
    }

    public F1Team(String teamNaam, String meestSuccesCoureur, Double winPercent, LocalDate eersteDeelname, Integer kampioenschappen, Hoofdkwartier hoofdkwartier) {
        super(teamNaam, eersteDeelname, hoofdkwartier);
        setMeestSuccesCoureur(meestSuccesCoureur);
        setWinPercent(winPercent);
        setKampioenschappen(kampioenschappen);
    }

    @CanRun("Michael Schumacher")
    public void setMeestSuccesCoureur(String meestSuccesCoureur) {
        if (!meestSuccesCoureur.isEmpty()) {
            this.meestSuccesCoureur = meestSuccesCoureur;
        } else throw new IllegalArgumentException("Coureurnaam kan niet leeg zijn");
    }

    @CanRun
    public void setWinPercent(Double winPercent) {
        if (winPercent >= 0 && winPercent <= 1) {
            this.winPercent = winPercent;
        } else throw new IllegalArgumentException("Geef een waarde tussen 0 en 1 voor het percentage");
    }

    @CanRun
    public void setKampioenschappen(Integer kampioenschappen) {
        if (kampioenschappen >= 0 && LocalDate.now().getYear() - this.eersteDeelname.getYear() >= kampioenschappen) {
            this.kampioenschappen = kampioenschappen;
        } else
            throw new IllegalArgumentException("Het aantal kampioenschappen kan niet minder dan 0 zijn of hoger dan het aantal seizoenen dat het team deelneemt");
    }

    //</editor-fold>

    //<editor-fold desc="Getters">

    public String getMeestSuccesCoureur() {
        return meestSuccesCoureur;
    }

    public Double getWinPercent() {
        return winPercent;
    }

    public Integer getKampioenschappen() {
        return kampioenschappen;
    }

    //</editor-fold>


//Functies

    @Override
    public int hashCode() {
        return Objects.hash(this.teamNaam);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        F1Team f1Team = (F1Team) obj;
        return Objects.equals(this.teamNaam, f1Team.teamNaam);
    }

    @Override
    public String toString() {
        return String.format("%30s%44s%s%34s%s%40s%d\n", teamNaam, "Succesvolste coureur: ", meestSuccesCoureur, "Eerste Deelname: ", eersteDeelname.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), "Aantal Kampioenschappen: ", kampioenschappen);
    }

}
