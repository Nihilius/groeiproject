package be.kdg.f1groeiproject.model;

import be.kdg.f1groeiproject.CanRun;

import java.time.LocalDate;

public class Team implements Comparable {
    protected String teamNaam;
    protected LocalDate eersteDeelname;
    private Hoofdkwartier hoofdkwartier;

    public Team(){
        setTeamNaam("Team");
        setEersteDeelname(LocalDate.now());
        setHoofdkwartier(Hoofdkwartier.UK);
    }

    public Team(String teamNaam, LocalDate eersteDeelname, Hoofdkwartier hoofdkwartier) {
        setTeamNaam(teamNaam);
        setEersteDeelname(eersteDeelname);
        setHoofdkwartier(hoofdkwartier);
    }

    //<editor-fold desc="Setters">
    @CanRun ("Scuderia Ferrari")
    public void setTeamNaam(String teamNaam) {
        if (teamNaam != null) {
            this.teamNaam = teamNaam;
        } else throw new IllegalArgumentException("Teamnaam kan niet leeg zijn");
    }

    @CanRun
    public void setEersteDeelname(LocalDate eersteDeelname) {
        if (eersteDeelname.compareTo(LocalDate.now()) <= 0) {
            this.eersteDeelname = eersteDeelname;
        } else throw new IllegalArgumentException("De datum van eerste deelname moet in het verleden liggen");
    }
    @CanRun
    public void setHoofdkwartier(Hoofdkwartier hoofdkwartier) {
        this.hoofdkwartier = hoofdkwartier;
    }

    public String getTeamNaam() {
        return teamNaam;
    }

    public LocalDate getEersteDeelname() {
        return eersteDeelname;
    }

    public Hoofdkwartier getHoofdkwartier() {
        return hoofdkwartier;
    }

    @Override
    public int compareTo(Object o) {
        return this.teamNaam.compareTo(((F1Team) o).teamNaam);
    }
}
