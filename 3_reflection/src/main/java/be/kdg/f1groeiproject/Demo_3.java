package be.kdg.f1groeiproject;


import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import be.kdg.f1groeiproject.model.Team;
import be.kdg.f1groeiproject.reflection.ReflectionTools;

public class Demo_3 {
    public static void main(String[] args) {
        ReflectionTools.classAnyalis(Team.class, F1Team.class, F1Teams.class);
        Object object = ReflectionTools.runAnnotated(F1Team.class);
        System.out.println(object);

    }
}
