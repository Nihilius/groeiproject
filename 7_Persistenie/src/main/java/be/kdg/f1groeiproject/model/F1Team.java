package be.kdg.f1groeiproject.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class F1Team implements Comparable, Serializable {
    public int id; //PK
    private String teamNaam;
    private String meestSuccesCoureur;
    private transient Double winPercent;
    private LocalDate eersteDeelname;
    private Integer kampioenschappen;
    private transient Hoofdkwartier hoofdkwartier;
    private final long serialVersionUID = 1L;

    //Constructors

    public F1Team() {
        new F1Team("Team", "John Doe", 0.0, LocalDate.now(), 0, Hoofdkwartier.UK);
    }

    public F1Team(String teamNaam, String meestSuccesCoureur, Double winPercent, LocalDate eersteDeelname, Integer kampioenschappen, Hoofdkwartier hoofdkwartier) {
        this.id = -1;
        setTeamNaam(teamNaam);
        setMeestSuccesCoureur(meestSuccesCoureur);
        setWinPercent(winPercent);
        setEersteDeelname(eersteDeelname);
        setKampioenschappen(kampioenschappen);
        setHoofdkwartier(hoofdkwartier);
    }

    public F1Team(int id, String teamNaam, String meestSuccesCoureur, Double winPercent, LocalDate eersteDeelname, Integer kampioenschappen, Hoofdkwartier hoofdkwartier) {
        this.id = id;
        setTeamNaam(teamNaam);
        setMeestSuccesCoureur(meestSuccesCoureur);
        setWinPercent(winPercent);
        setEersteDeelname(eersteDeelname);
        setKampioenschappen(kampioenschappen);
        setHoofdkwartier(hoofdkwartier);
    }


    //<editor-fold desc="Setters">
    public void setId(int id){
        this.id=id;
    }

    public void setTeamNaam(String teamNaam) {
        if (!teamNaam.isEmpty()) {
            this.teamNaam = teamNaam;
        } else throw new IllegalArgumentException("Teamnaam kan niet leeg zijn");
    }

    public void setMeestSuccesCoureur(String meestSuccesCoureur) {
        if (!meestSuccesCoureur.isEmpty()) {
            this.meestSuccesCoureur = meestSuccesCoureur;
        } else throw new IllegalArgumentException("Coureurnaam kan niet leeg zijn");
    }

    public void setWinPercent(Double winPercent) {
        if (winPercent >= 0 && winPercent <= 1) {
            this.winPercent = winPercent;
        } else throw new IllegalArgumentException("Geef een waarde tussen 0 en 1 voor het percentage");
    }

    public void setEersteDeelname(LocalDate eersteDeelname) {
        if (eersteDeelname.compareTo(LocalDate.now()) <= 0) {
            this.eersteDeelname = eersteDeelname;
        } else throw new IllegalArgumentException("De datum van eerste deelname moet in het verleden liggen");
    }

    public void setKampioenschappen(Integer kampioenschappen) {
        if (kampioenschappen >= 0 && LocalDate.now().getYear() - this.eersteDeelname.getYear() >= kampioenschappen) {
            this.kampioenschappen = kampioenschappen;
        } else
            throw new IllegalArgumentException("Het aantal kampioenschappen kan niet minder dan 0 zijn of hoger dan het aantal seizoenen dat het team deelneemt");
    }

    public void setHoofdkwartier(Hoofdkwartier hoofdkwartier) {
        this.hoofdkwartier = hoofdkwartier;
    }
    //</editor-fold>

    //<editor-fold desc="Getters">

    public int getId(){
        return id;
    }

    public String getTeamNaam() {
        return teamNaam;
    }

    public String getMeestSuccesCoureur() {
        return meestSuccesCoureur;
    }

    public Double getWinPercent() {
        return winPercent;
    }

    public LocalDate getEersteDeelname() {
        return eersteDeelname;
    }

    public Integer getKampioenschappen() {
        return kampioenschappen;
    }

    public Hoofdkwartier getHoofdkwartier() {
        return hoofdkwartier;
    }
    //</editor-fold>


//Functies

    @Override
    public int hashCode() {
        return Objects.hash(this.teamNaam);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        F1Team f1Team = (F1Team) obj;
        return Objects.equals(this.teamNaam, f1Team.teamNaam);
    }

    @Override
    public String toString() {
        return String.format("%30s%44s%s%34s%s%40s%d\n", teamNaam, "Succesvolste coureur: ", meestSuccesCoureur, "Eerste Deelname: ", eersteDeelname.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), "Aantal Kampioenschappen: ", kampioenschappen);
    }


    @Override
    public int compareTo(Object o) {
        return this.teamNaam.compareTo(((F1Team) o).teamNaam);
    }
}
