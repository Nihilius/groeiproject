package be.kdg.f1groeiproject.persist;

import be.kdg.f1groeiproject.model.F1Teams;

import javax.management.ObjectInstance;
import java.io.*;

public class F1TeamsSerializer {
    private final String FILENAME = "db\\f1teams.ser";

    public void serialize (F1Teams f1Teams) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(FILENAME);
        ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
        objectOut.writeObject(f1Teams);
        fileOut.close();
        objectOut.close();
    }

    public F1Teams deserialize() throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(FILENAME);
        ObjectInputStream objectIn = new ObjectInputStream(fileIn);
        return (F1Teams) objectIn.readObject();
    }
}
