package be.kdg.f1groeiproject.persist;

import be.kdg.f1groeiproject.model.F1Team;

import java.util.List;

public interface F1TeamDao {
    public abstract boolean insert(F1Team f1Team);

    public abstract boolean delete(String naam);

    public abstract boolean update(F1Team f1Team);

    public abstract F1Team retrieve(String naam);

    public abstract List<F1Team> sortedOn(String query);

}
