package be.kdg.f1groeiproject.persist;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.Hoofdkwartier;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class F1TeamDbDao implements F1TeamDao {
    private Connection connection;
    private static F1TeamDbDao enigeF1TeamDbDao;

    private F1TeamDbDao(String databasePath) {
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePath, "sa", "");
            System.out.println("Connectie met DB gemaakt");
        } catch (SQLException e) {
            System.err.println("Kan geen connectie maken met DB " + databasePath);
            System.exit(1);
        }
        createTable();
    }

    public static synchronized F1TeamDbDao getInstance(){
        if (enigeF1TeamDbDao == null){
            enigeF1TeamDbDao = new F1TeamDbDao("db/f1teamdb");
        }
        return enigeF1TeamDbDao;
    }

    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.err.println("Probleem met sluiten connectie naar DB");
        }
    }

    private void createTable() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE f1teamtable IF EXISTS ");
            String createTableQuery = "CREATE TABLE f1teamtable " +
                    "(id INTEGER NOT NULL IDENTITY, " +
                    "teamnaam VARCHAR(50) NOT NULL," +
                    "meestSuccesCoureur VARCHAR(50) NOT NULL," +
                    "winPercent DOUBLE," +
                    "eersteDeelname DATE," +
                    "kampioenschappen INTEGER," +
                    "hoofdkwartier VARCHAR(5))";
            statement.execute(createTableQuery);
            System.out.println("Tabel aangemaakt");
            statement.close();
        } catch (SQLException e) {
            String message = e.getMessage();
            if (message.contains("Tabel bestaat al")) return;
            System.err.println("Onverwachte fout bij aanmaken tabel: " + e.getMessage());
            System.exit(1);
        }
    }

    @Override
    public boolean insert(F1Team f1Team) {
        if (f1Team.getId() >= 0) return false;
        try {
            PreparedStatement prepStatement = connection.prepareStatement(
                    "INSERT INTO f1teamtable (id, teamnaam, meestSuccesCoureur, winPercent, eersteDeelname, kampioenschappen, hoofdkwartier) "
                            + "VALUES(NULL, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            prepStatement.setString(1, f1Team.getTeamNaam());
            prepStatement.setString(2, f1Team.getMeestSuccesCoureur());
            prepStatement.setDouble(3, f1Team.getWinPercent());
            prepStatement.setDate(4, Date.valueOf(f1Team.getEersteDeelname()));
            prepStatement.setInt(5, f1Team.getKampioenschappen());
            prepStatement.setString(6, f1Team.getHoofdkwartier().name());
            boolean result = prepStatement.executeUpdate() == 1;
            prepStatement.close();
            return result;
        } catch (SQLException e) {
            System.err.println("Fout bij het toevoegen in DB " + e);
            return false;
        }
    }

    @Override
    public boolean delete(String naam) {
        try {
            Statement statement = connection.createStatement();
            if (naam.equals("*")) {
                statement.execute("DELETE FROM f1teamtable");
            } else {
                statement.execute("DELETE FROM f1teamtable WHERE teamnaam='" + naam + "'");
            }
            statement.close();
            return true;
        } catch (SQLException e) {
            System.err.println("Probleem met het verwijderen van element " + e);
            return false;
        }
    }

    @Override
    public boolean update(F1Team f1Team) {
        try {
            PreparedStatement prepStatement = connection.prepareStatement("UPDATE f1teamtable " +
                    "SET teamnaam = ?, " +
                    "meestSuccesCoureur =?, " +
                    "winPercent=?, " +
                    "eersteDeelname =?, " +
                    "kampioenschappen =?, " +
                    "hoofdkwartier=? " +
                    "WHERE id=" + f1Team.getId());
            prepStatement.setString(1, f1Team.getTeamNaam());
            prepStatement.setString(2, f1Team.getMeestSuccesCoureur());
            prepStatement.setDouble(3, f1Team.getWinPercent());
            prepStatement.setDate(4, Date.valueOf(f1Team.getEersteDeelname()));
            prepStatement.setInt(5, f1Team.getKampioenschappen());
            prepStatement.setString(6, f1Team.getHoofdkwartier().name());
            prepStatement.execute();
            prepStatement.close();
            return true;
        } catch (SQLException e) {
            System.err.println("Probleem met updaten data " + e);
            return false;
        }
    }

    /*TODO: Retrieve functie nullPointerException fixen
    Big oof
     */

    @Override
    public F1Team retrieve(String naam) {
        F1Team team = null;
        ResultSet res;
        try {
            Statement statement = connection.createStatement();
            res = statement.executeQuery("SELECT * FROM f1teamtable WHERE teamnaam = '" + naam + "'");
            while (res.next()) {
                team = new F1Team(res.getInt("id"),
                        res.getString("teamnaam"),
                        res.getString("meestSuccesCoureur"),
                        res.getDouble("winPercent"),
                        res.getDate("eersteDeelname").toLocalDate(),
                        res.getInt("kampioenschappen"),
                        Hoofdkwartier.valueOf(res.getString("hoofdkwartier"))
                );
            }
            statement.close();
        } catch (SQLException e) {
            System.err.println("Fout bij uitlezen object uit DB " + e);
            return null;
        }
        return team;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    @Override
    public List<F1Team> sortedOn(String query) {
        List<F1Team> teams = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM f1teamtable ORDER BY " + query);
            while (resultSet.next()) {
                teams.add(new F1Team(resultSet.getString("teamnaam"),
                        resultSet.getString("meestSuccesCoureur"),
                        resultSet.getDouble("winPercent"),
                        resultSet.getDate("eersteDeelname").toLocalDate(),
                        resultSet.getInt("kampioenschappen"),
                        Hoofdkwartier.valueOf(resultSet.getString("hoofdkwartier"))));
            }
            statement.close();
        } catch (SQLException e) {
            System.err.println("Probleem met het uitlezen van de lijst" + e);
        }
        return teams;
    }
}