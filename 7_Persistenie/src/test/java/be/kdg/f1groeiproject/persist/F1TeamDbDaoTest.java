package be.kdg.f1groeiproject.persist;

import be.kdg.f1groeiproject.data.Data;
import be.kdg.f1groeiproject.model.F1Team;
import org.junit.*;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class F1TeamDbDaoTest {
    private static F1TeamDbDao f1TeamDbDao;
    List<F1Team> data;

    @BeforeClass
    public static void initiate() {
        f1TeamDbDao = F1TeamDbDao.getInstance();
    }

    @AfterClass
    public static void close() {
        f1TeamDbDao.close();
    }

    @Before
    public void setUp() {
        data = Data.getData();
        data.forEach(f1Team -> f1TeamDbDao.insert(f1Team));
    }

    @After
    public void removeAll() {
        f1TeamDbDao.delete("*");
        data = null;
    }

    @Test
    public void testInsert() {
        Assert.assertEquals(data.size(), f1TeamDbDao.sortedOn("teamnaam").size());
    }

    @Test
    public void testRetrieveUpdate() {
            F1Team ferrari = f1TeamDbDao.retrieve("Scuderia Ferrari");
            ferrari.setTeamNaam("Ferrari");
            Assert.assertTrue(f1TeamDbDao.update(ferrari));
            F1Team test = f1TeamDbDao.retrieve("Ferrari");
            Assert.assertEquals("Ferrari", test.getTeamNaam());
    }


    @Test
    public void testDelete() {
        Assert.assertTrue(f1TeamDbDao.delete("Red Bull Racing"));
    }

    @Test
    public void testSort() {
        data.sort(F1Team::compareTo);
        assertEquals(data, f1TeamDbDao.sortedOn("teamnaam"));
    }

    @Test
    public void testSingleton() {
        assertEquals(f1TeamDbDao, F1TeamDbDao.getInstance());
        try {
            f1TeamDbDao.clone();
        } catch (CloneNotSupportedException e) {
            Assert.assertNotNull(e);
        }
    }
}
