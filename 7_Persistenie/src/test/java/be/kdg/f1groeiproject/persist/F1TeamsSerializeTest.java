package be.kdg.f1groeiproject.persist;

import be.kdg.f1groeiproject.data.Data;
import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.*;

public class F1TeamsSerializeTest {
    private F1Teams teams = new F1Teams();
    private F1TeamsSerializer serializer = new F1TeamsSerializer();

    @Before
    public void setUp(){
        List<F1Team> lijst = Data.getData();
        lijst.forEach(item -> teams.voegToe(item));
    }

    @Test()
    public void testSerialize() {
        try {
            serializer.serialize(teams);
        } catch (IOException e){
            fail("Er mag geen IOException optreden");
        }
    }

    @Test
    public void testDeserialize() {
        try {
            F1Teams deserialized = serializer.deserialize();
            assertEquals(teams.gesorteerdOpNaam(), deserialized.gesorteerdOpNaam());
        } catch (IOException | ClassNotFoundException e){
            fail("Er zou geen exception mogen optreden");
        }
    }
}
