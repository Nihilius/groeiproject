package be.kdg.f1groeiproject.generics;

import javax.swing.*;
import java.util.*;


public class PriorityQueue<T> implements FIFOQueue<T> {
    private LinkedList<T> linkedList1;
    private LinkedList<T> linkedList2;
    private LinkedList<T> linkedList3;
    private LinkedList<T> linkedList4;
    private LinkedList<T> linkedList5;
    private TreeMap<Integer, LinkedList<T>> treeMap;

    public PriorityQueue() {
        treeMap = new TreeMap<>(Comparator.reverseOrder());
        linkedList1 = new LinkedList<>();
        linkedList2 = new LinkedList<>();
        linkedList3 = new LinkedList<>();
        linkedList4 = new LinkedList<>();
        linkedList5 = new LinkedList<>();
    }

    @Override
    public String toString() {
        String inhoud = "";

        for (Map.Entry<Integer, LinkedList<T>> entry : treeMap.entrySet()) {
            String inhoudValue ="";
            for(int i =0; i<entry.getValue().size(); i++){
                inhoudValue += entry.getValue().get(i).toString() + '\n';
            }
            inhoud += entry.getKey() + ": " + inhoudValue + "\n";

        }
        return inhoud;
    }


    @Override
    public boolean enqueue(T element, int priority) {
        boolean toegevoegd = true;
        {
            switch (priority) {
                case 1:
                    if (linkedList1.contains(element)){
                        toegevoegd = false;
                    } else{
                    linkedList1.add(element);
                    if (treeMap.get(priority) == null) {
                        treeMap.put(priority, linkedList1);
                        toegevoegd = true;
                    }
                    }
                    break;
                case 2:
                    if (linkedList2.contains(element)){
                        toegevoegd = false;
                    } else{
                        linkedList2.add(element);
                        if (treeMap.get(priority) == null) {
                            treeMap.put(priority, linkedList2);
                            toegevoegd = true;
                        }
                    }
                    break;
                case 3:
                    if (linkedList3.contains(element)){
                        toegevoegd = false;
                    } else{
                        linkedList3.add(element);
                        if (treeMap.get(priority) == null) {
                            treeMap.put(priority, linkedList3);
                            toegevoegd = true;
                        }
                    }
                    break;
                case 4:
                    if (linkedList4.contains(element)){
                        toegevoegd = false;
                    } else{
                        linkedList4.add(element);
                        if (treeMap.get(priority) == null) {
                            treeMap.put(priority, linkedList4);
                            toegevoegd = true;
                        }
                    }
                    break;
                case 5:
                    if (linkedList5.contains(element)){
                        toegevoegd = false;
                    } else{
                        linkedList5.add(element);
                        if (treeMap.get(priority) == null) {
                            treeMap.put(priority, linkedList5);
                            toegevoegd = true;
                        }
                    }
                    break;
            }

        }
        return toegevoegd;
    }


    @Override
    public T dequeue() {
        T element = null;
        element = treeMap.firstEntry().getValue().getFirst();
        treeMap.firstEntry().getValue().remove(element);
        if(treeMap.firstEntry().getValue().size() == 0){
            treeMap.remove(treeMap.firstKey());
        }
        return element;
    }

    @Override
    public int search(T element) {
        int elementFound = 0;
        for (Map.Entry<Integer, LinkedList<T>> entry : treeMap.entrySet()) {
            if (entry.getValue().contains(element)) {
                    if (entry.getKey() == 5) {
                        elementFound = 6 - entry.getKey()+entry.getValue().indexOf(element);
                    } else {
                        for (int i=5; i!=entry.getKey();i--){
                            if(treeMap.get(i) !=null) {
                                elementFound += treeMap.get(i).size();
                            }
                        }
                        elementFound += entry.getValue().indexOf(element);
                        elementFound += 2;
                    }


            }
            if (elementFound == 0) elementFound = -1;

        }return elementFound;
    }

    @Override
    public int getSize() {
        int size = 0;
        for (Map.Entry<Integer, LinkedList<T>> entry : treeMap.entrySet()) {
            size += entry.getValue().size();
        }
        return size;
    }
}
