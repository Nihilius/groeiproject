package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.data.Data;
import be.kdg.f1groeiproject.generics.PriorityQueue;
import be.kdg.f1groeiproject.model.F1Team;

import java.util.List;
import java.util.Random;

public class Demo_2 {
    public static void main(String[] args) {
        var f1Queue = new PriorityQueue<>();
        var f1Teams = new Data().getData();
        var random = new Random();
        for(F1Team team : f1Teams){
            f1Queue.enqueue(team, random.nextInt(5)+1);
        }


        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(f1Queue.toString());
        System.out.println("aantal: " + f1Queue.getSize());
        System.out.println("positie van " + f1Teams.get(5).getTeamNaam()+ " : " + f1Queue.search(f1Teams.get(5)));

        for (int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + f1Queue.dequeue());
        }
        System.out.println("Size na dequeue: " + f1Queue.getSize());
    }
}