package kdg.f1groeiproject.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestF1Team.class, TestF1Teams.class})
public class TestSuite {

}
