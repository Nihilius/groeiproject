package kdg.f1groeiproject.model;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestF1Teams {
    private F1Teams f1Teams = new F1Teams();
    private F1Team redBull;
    private F1Team ferrari;
    private F1Team brabham;
    private F1Team redBull2;

    @Before
    public void setup() {
        f1Teams = new F1Teams();
        redBull = new F1Team("Red Bull Racing", "Sebastian Vettel", 0.21678,  LocalDate.of(2005,3,6), 4, Hoofdkwartier.UK);
        redBull2 = new F1Team("Red Bull Racing", "Sebastian Vettel", 0.21678,  LocalDate.of(2005,3,6), 4, Hoofdkwartier.UK);
        ferrari = new F1Team("Scuderia Ferrari", "Michael Schumacher", 0.23939,  LocalDate.of(1950,5,21), 16, Hoofdkwartier.ITALY);
        brabham = new F1Team("Brabham", "Nelson Piquet", 0.08883,  LocalDate.of(1962,8,5), 2, Hoofdkwartier.UK);
        f1Teams.voegToe(redBull);
        f1Teams.voegToe(ferrari);
    }

    @Test
    public void testToevoegen() {
        assertTrue("Het object zit nog niet in de lijst dus moet kunnen worden toegevoegd", f1Teams.voegToe(brabham));
        assertFalse("Je kan geen team toevoegen dat al in de lijst zit. ", f1Teams.voegToe(redBull2));
    }

    @Test
    public void testVerwijderen() {
        assertTrue("Het object zit in de lijst dus moet kunnen verwijderd worden", f1Teams.verwijder("Red Bull Racing"));
        assertFalse("Het object zit niet in de lijst dus verwijder functie kan geen true returnen", f1Teams.verwijder("Brabham"));
    }


}