package kdg.f1groeiproject.model;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.channels.IllegalChannelGroupException;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class TestF1Team {
    private F1Team team1;
    private F1Team team2;
    private F1Team ferrari;

    @Before
    public void setUp() {
        team1 = new F1Team("Scuderia Ferrari", "Michael Schumacher", 0.23939, LocalDate.of(1950, 5, 21), 16, Hoofdkwartier.ITALY);
        team2 = new F1Team();
        ferrari = new F1Team("Scuderia Ferrari", "Michael Schumacher", 0.23939, LocalDate.of(1950, 5, 21), 16, Hoofdkwartier.ITALY);
    }

    @Test
    public void testEquals() {
        assertTrue("De teams zouden aan elkaar gelijk moeten zijn", team1.equals(ferrari));
        assertFalse("De teams mogen niet aan elkaar gelijk zijn", team1.equals(team2));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testIllegalWinPercent(){
            team2.setWinPercent(5.0);
            fail("Het opgeven van waarde buiten de range 0-1 zou een exception moeten geven");
    }

    @Test
    public void testLegalName(){
        try {
            team2.setTeamNaam("Red Bull Racing");
        } catch (IllegalArgumentException e){
            fail("Het opgeven van een geldige naam zou geen exception mogen gooien: " + e.toString());
        }
    }

    @Test
    public void testCompareTo(){
        assertTrue("Het eerste team zou alfabetisch voor het 2e moeten komen", ferrari.compareTo(team2) < 0);
        assertTrue("Beide teams zouden alfabetisch gelijk moeten zijn", ferrari.compareTo(team1) == 0);
    }

    @Test
    public void testWinPercentages() {
        assertEquals("De winpercentages zouden aan elkaar gelijk moeten zijn", team1.getWinPercent(), ferrari.getWinPercent(), 0.0);
    }

}