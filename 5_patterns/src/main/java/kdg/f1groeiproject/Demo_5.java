package kdg.f1groeiproject;

import kdg.f1groeiproject.model.F1Team;
import kdg.f1groeiproject.model.F1TeamFactory;
import kdg.f1groeiproject.model.Hoofdkwartier;
import kdg.f1groeiproject.patterns.F1TeamsObserver;
import kdg.f1groeiproject.patterns.ObservableF1Teams;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.stream.Stream;

public class Demo_5 {
    public static void main(String[] args) {
        ObservableF1Teams observableF1Teams = new ObservableF1Teams();
        F1TeamsObserver f1TeamsObserver = new F1TeamsObserver();
        F1Team redBull = F1TeamFactory.newFilledF1Team("Red Bull Racing", "Sebastian Vettel", 0.10, LocalDate.parse("2005-03-10"), 4, Hoofdkwartier.UK);
        F1Team ferrari = F1TeamFactory.newFilledF1Team("Scuderia Ferrari", "Michael Schumacher", 0.24, LocalDate.parse("1950-06-01"), 16, Hoofdkwartier.ITALY);

        observableF1Teams.addObserver(f1TeamsObserver);

        observableF1Teams.voegToe(redBull);
        observableF1Teams.voegToe(ferrari);


        System.out.println("Empty team: ");
        System.out.println(F1TeamFactory.newEmptyF1Team().toString());
        System.out.println("\nFilled team: ");
        System.out.println(F1TeamFactory.newFilledF1Team("Scuderia Ferrari", "Michael Schumacher", 0.24, LocalDate.of(1950, 5, 13), 16, Hoofdkwartier.ITALY));
        System.out.println("\n30 random gegeneerde team, gesorteerd op naam: ");
        Stream.generate(() -> F1TeamFactory.newRandomF1Team())
                .limit(30)
                .sorted()
                .forEach(System.out::println);
    }
}
