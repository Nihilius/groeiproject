package kdg.f1groeiproject.model;

import java.util.List;

public interface F1TeamsInterface {
    boolean voegToe(F1Team team);

    boolean verwijder(String teamnaam);

    F1Team zoek(String teamnaam);

    List<F1Team> gesorteerdOpNaam();

    List<F1Team> gesorteerdOpEersteDeelname();

    List<F1Team> gesorteerdOpKampioenschappen();

    int getAantal();
}
