package kdg.f1groeiproject.patterns;

import kdg.f1groeiproject.model.F1Team;
import kdg.f1groeiproject.model.F1Teams;
import kdg.f1groeiproject.model.F1TeamsInterface;

import java.util.List;
import java.util.Observable;

public class ObservableF1Teams extends Observable implements F1TeamsInterface {
    private F1Teams f1Teams = new F1Teams();

    @Override
    public boolean voegToe(F1Team team) {
        boolean bool = f1Teams.voegToe(team);
        setChanged();
        notifyObservers(team);
        return bool ;
    }

    @Override
    public boolean verwijder(String teamnaam) {
        boolean bool = f1Teams.verwijder(teamnaam);
        setChanged();
        notifyObservers(f1Teams.zoek(teamnaam));
        return bool;
    }

    @Override
    public F1Team zoek(String teamnaam) {
        return f1Teams.zoek(teamnaam);
    }

    @Override
    public List<F1Team> gesorteerdOpNaam() {
        return f1Teams.gesorteerdOpNaam();
    }

    @Override
    public List<F1Team> gesorteerdOpEersteDeelname() {
        return f1Teams.gesorteerdOpEersteDeelname();
    }

    @Override
    public List<F1Team> gesorteerdOpKampioenschappen() {
        return f1Teams.gesorteerdOpKampioenschappen();
    }

    @Override
    public int getAantal() {
        return f1Teams.getAantal();
    }
}
