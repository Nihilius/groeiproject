package kdg.f1groeiproject.patterns;

import java.util.Observable;
import java.util.Observer;

public class F1TeamsObserver implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Observer meldt: Toegevoegd " + arg.toString());
    }
}
