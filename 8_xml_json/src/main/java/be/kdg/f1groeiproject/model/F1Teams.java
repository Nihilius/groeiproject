package be.kdg.f1groeiproject.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement
public class F1Teams {
    private List<F1Team> f1teams = new ArrayList<>();

    public boolean voegToe(F1Team team) {
        boolean nieuw = true;

        for (F1Team f1Team : getF1teamList()) {
            if (f1Team.equals(team) && f1Team.hashCode() == team.hashCode()) {
                nieuw = false;
                break;
            }
        }
        if (nieuw) {
            getF1teamList().add(team);
            return nieuw;
        }
        return nieuw;
    }

    public boolean verwijder(String teamnaam) {
        Iterator iterator = getF1teamList().iterator();
        while (iterator.hasNext()) {
            Object team = iterator.next();
            if (((F1Team) team).getTeamNaam().equals(teamnaam)) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public F1Team zoek(String teamnaam) {
        for (F1Team f1team : getF1teamList()) {
            if (f1team.getTeamNaam().equals(teamnaam)) {
                return f1team;
            }
        }
        return null;
    }

    public List<F1Team> gesorteerdOpNaam() {
        List<F1Team> lijstOpNaam = new ArrayList<>(getF1teamList());
        return lijstOpNaam;

    }

    public List<F1Team> gesorteerdOpEersteDeelname() {
        class SorteerOpDeelname implements Comparator<F1Team> {


            @Override
            public int compare(F1Team o1, F1Team o2) {
                return o1.getEersteDeelname().compareTo(o2.getEersteDeelname());
            }
        }
        List<F1Team> sorteerOpDeelName = new ArrayList<>(getF1teamList());

        Collections.sort(sorteerOpDeelName, new SorteerOpDeelname());

        return sorteerOpDeelName;

    }

    public List<F1Team> gesorteerdOpKampioenschappen() {
        class SorteerOpKampioen implements Comparator<F1Team> {

            @Override
            public int compare(F1Team o1, F1Team o2) {
                return o1.getKampioenschappen().compareTo(o2.getKampioenschappen());
            }
        }
        List<F1Team> sorteerOpKampioenschappen = new ArrayList<>(getF1teamList());
        Collections.sort(sorteerOpKampioenschappen, new SorteerOpKampioen());
        Collections.reverse(sorteerOpKampioenschappen);
        return sorteerOpKampioenschappen;
    }

    public int getAantal() {
        return getF1teamList().size();
    }

    public List<F1Team> getF1teamList() {
        return f1teams;
    }

    @XmlElement(name = "f1team")
    public void setF1teamList(List<F1Team> f1teams) {
        this.f1teams = f1teams;
    }
}
