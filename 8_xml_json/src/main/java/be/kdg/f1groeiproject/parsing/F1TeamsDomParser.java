package be.kdg.f1groeiproject.parsing;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import be.kdg.f1groeiproject.model.Hoofdkwartier;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

public class F1TeamsDomParser {
    public static F1Teams domReadXml(String filename) throws ParserConfigurationException, SAXException, IOException {
        //Create Document
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(filename));

        //Extract root element
        Element rootElement = doc.getDocumentElement();
        System.out.println("Root element: " + rootElement.getNodeName());
        F1Teams f1Teams = new F1Teams();
        NodeList teamNodes = rootElement.getChildNodes();
        for(int i=0; i<teamNodes.getLength(); i++){
            if(teamNodes.item(i).getNodeType() != Node.ELEMENT_NODE){
                continue;
            }
            Element element = (Element) teamNodes.item(i);
            String hq = element.getAttribute("locatie");
            Element teamnaam = (Element) element.getElementsByTagName("Teamnaam").item(0);
            Element coureur = (Element) element.getElementsByTagName("Meest-succesvolle-coureur").item(0);
            Element wins = (Element) element.getElementsByTagName("Winst-percentage").item(0);
            Element eersteDeelname = (Element) element.getElementsByTagName("Eerste-deelname").item(0);
            Element kampioenschap = (Element) element.getElementsByTagName("Kampioenschappen").item(0);

            f1Teams.voegToe(new F1Team(teamnaam.getTextContent(), coureur.getTextContent(), Double.parseDouble(wins.getTextContent()),
                    LocalDate.parse(eersteDeelname.getTextContent()), Integer.parseInt(kampioenschap.getTextContent()), Hoofdkwartier.valueOf(hq)));
        }
        return f1Teams;
    }
}
