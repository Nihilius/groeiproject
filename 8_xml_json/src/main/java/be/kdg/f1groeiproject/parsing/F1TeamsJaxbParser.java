package be.kdg.f1groeiproject.parsing;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class F1TeamsJaxbParser {
    public static void jaxbWriteXml(String file, Object root) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(root.getClass());

        Marshaller marshal = context.createMarshaller();
        marshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        marshal.marshal(root, new File(file));
        System.out.println("File created");
    }

    public static <T> T jaxbReadXml(String filePath, Class<T> typeParameterClass) throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(typeParameterClass);
        Unmarshaller unmarshal = context.createUnmarshaller();

        File file = new File(filePath);

        T object = (T) unmarshal.unmarshal(file);
        System.out.println("Unmarshal complete");
        return object;
    }
}
