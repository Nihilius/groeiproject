package be.kdg.f1groeiproject.parsing;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class F1TeamsGsonParser {
    public static void writeJson(F1Teams f1Teams, String fileName){
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.setPrettyPrinting().create();

        String jsonString = gson.toJson(f1Teams);

        try(FileWriter jsonwriter = new FileWriter(fileName)){
            jsonwriter.write(jsonString);
        } catch (IOException e){
            System.err.println("ERROR IN JSON SCHRIJVEN " + e);
        }
    }

    public static F1Teams readJson(String filename){
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.setPrettyPrinting().create();
        try(BufferedReader data = new BufferedReader(new FileReader(filename))) {
            F1Teams f1Teams = gson.fromJson(data, F1Teams.class);
            return f1Teams;
        } catch (IOException e) {
            System.err.println("ERROR IN LEZEN JSON " + e);
            return null;
        }

    }
}
