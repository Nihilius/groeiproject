package be.kdg.f1groeiproject.parsing;

import be.kdg.f1groeiproject.data.Data;
import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class F1TeamsStaxParser {
    XMLStreamWriter xmlStreamWriter;
    List<F1Team> f1TeamList;

    public F1TeamsStaxParser(F1Teams f1Teams, String dataPath) throws IOException, XMLStreamException {
        this.f1TeamList = f1Teams.gesorteerdOpNaam();
        FileWriter writerXml = new FileWriter(dataPath);
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(writerXml);
        xmlStreamWriter = new IndentingXMLStreamWriter(xmlStreamWriter);

    }

    public void writeXml() throws XMLStreamException{
        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement("F1-teams");
        for (F1Team f1Team: f1TeamList){
            writeElement(f1Team);
        }
        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndDocument();
        xmlStreamWriter.close();

    }

    private void writeElement(F1Team f1Team) throws XMLStreamException {
        //Element maken
        xmlStreamWriter.writeStartElement("F1-team");
        xmlStreamWriter.writeAttribute("locatie", f1Team.getHoofdkwartier().name());
        //Teamnaam veld
        xmlStreamWriter.writeStartElement("Teamnaam");
        xmlStreamWriter.writeCharacters(f1Team.getTeamNaam());
        xmlStreamWriter.writeEndElement();
        //Coureur veld
        xmlStreamWriter.writeStartElement("Meest-succesvolle-coureur");
        xmlStreamWriter.writeCharacters(f1Team.getMeestSuccesCoureur());
        xmlStreamWriter.writeEndElement();
        //Winpercent veld
        xmlStreamWriter.writeStartElement("Winst-percentage");
        xmlStreamWriter.writeCharacters(String.valueOf(f1Team.getWinPercent()));
        xmlStreamWriter.writeEndElement();
        //Eerste Deelname
        xmlStreamWriter.writeStartElement("Eerste-deelname");
        xmlStreamWriter.writeCharacters(String.valueOf(f1Team.getEersteDeelname()));
        xmlStreamWriter.writeEndElement();
        //Kampioenschappen
        xmlStreamWriter.writeStartElement("Kampioenschappen");
        xmlStreamWriter.writeCharacters(String.valueOf(f1Team.getKampioenschappen()));
        xmlStreamWriter.writeEndElement();
        //Sluiten Team tag
        xmlStreamWriter.writeEndElement();
    }
}
