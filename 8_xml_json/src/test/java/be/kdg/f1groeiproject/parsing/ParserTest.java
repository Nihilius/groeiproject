package be.kdg.f1groeiproject.parsing;

import be.kdg.f1groeiproject.data.Data;
import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

public class ParserTest {
    private final String filePath = "datafiles/f1teams.xml";
    private F1Teams f1Teams;
    private F1TeamsStaxParser staxParser;

    @Before
    public void setUp() {
        f1Teams = new F1Teams();
        List<F1Team> teams = new Data().getData();
        for (F1Team f1Team : teams) {
            f1Teams.voegToe(f1Team);
        }
    }

    @Test
    public void testStaxDom() {
        try {
            staxParser = new F1TeamsStaxParser(f1Teams, filePath);
            staxParser.writeXml();
            F1Teams xmlTeams = F1TeamsDomParser.domReadXml(filePath);
            Assert.assertEquals(f1Teams.gesorteerdOpNaam(), xmlTeams.gesorteerdOpNaam());
        } catch (IOException | XMLStreamException | ParserConfigurationException | SAXException e) {
            System.err.println(e.getMessage());
            fail("Er zou geen excpetion mogen gegooid worden");
        }
    }

    @Test
    public void testJaxb(){
        try {
            F1TeamsJaxbParser.jaxbWriteXml("datafiles/f1teamsJAXB.xml", f1Teams);
            F1Teams xmlTeams = F1TeamsJaxbParser.jaxbReadXml("datafiles/f1teamsJAXB.xml", f1Teams.getClass());
            Assert.assertEquals(f1Teams.gesorteerdOpNaam(), xmlTeams.gesorteerdOpNaam());
        } catch (JAXBException e) {
            e.printStackTrace();
            fail("Geen exception mogelijk");
        }
    }
    @Test
    public void testGson() {
        F1TeamsGsonParser.writeJson(f1Teams, "datafiles/f1teams.json");
        F1Teams newF1Teams = F1TeamsGsonParser.readJson("datafiles/f1teams.json");
        Assert.assertEquals(f1Teams.gesorteerdOpNaam(), newF1Teams.gesorteerdOpNaam());
    }
}
