package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.data.Data;
import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1Teams;
import be.kdg.f1groeiproject.model.Hoofdkwartier;

import java.time.LocalDate;
import java.util.List;

public class Demo_1 {
    public static void main(String[] args) {
        F1Teams teams = new F1Teams();
        List<F1Team> data = new Data().getData();

        for (F1Team team : data) {
            teams.voegToe(team);
        }

        System.out.println(teams.verwijder("Cooper Car Company"));
        System.out.println(teams.zoek("Red Bull Racing"));
        System.out.println(teams.voegToe(new F1Team("Scuderia Ferrari", "Michael Schumacher", 0.23939, LocalDate.of(1950, 5, 21), 16, Hoofdkwartier.ITALY)));
        List<F1Team> naamSort = teams.gesorteerdOpNaam();
        System.out.println("\nGesorteerd op naam: \n");
        for (F1Team f1Team : naamSort) {
            System.out.format(f1Team.toString());
        }
        List<F1Team> dateSort = teams.gesorteerdOpEersteDeelname();
        System.out.println("\nGesorteerd op eerste deelname: \n");
        for (F1Team f1Team : dateSort) {
            System.out.format(f1Team.toString());
        }
        List<F1Team> champSort = teams.gesorteerdOpKampioenschappen();
        System.out.println("\nGesorteerd op kampioenschappen: \n");
        for (F1Team f1Team : champSort) {
            System.out.format(f1Team.toString());
        }
        System.out.format("\nAantal teams in de lijst: %d\n", teams.getAantal());
    }

}
