package be.kdg.f1groeiproject.model;

import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class F1TeamFactory {
    private F1TeamFactory() {
    }

    public static F1Team newEmptyF1Team() {
        return new F1Team();
    }

    public static F1Team newFilledF1Team(String teamnaam, String coureur, Double winPercent, LocalDate eersteDeelname, Integer kampioenschappen, Hoofdkwartier hoofdkwartier) {
        return new F1Team(teamnaam, coureur, winPercent, eersteDeelname, kampioenschappen, hoofdkwartier);
    }

    public static F1Team newRandomF1Team() {
        Random random = new Random();
        long minDay = LocalDate.of(1950, 5, 13).toEpochDay();
        long maxDay = LocalDate.of(2020, 3, 15).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        Hoofdkwartier randomPlaats;
        int randomEnum = random.nextInt(3);
        if (randomEnum == 0) {
            randomPlaats = Hoofdkwartier.UK;
        } else if (randomEnum == 1) {
            randomPlaats = Hoofdkwartier.ITALY;
        } else {
            randomPlaats = Hoofdkwartier.US;
        }
        return new F1Team(generateString(10, 3),
                generateString(8, 2),
                random.nextDouble(),
                LocalDate.ofEpochDay(randomDay),
                random.nextInt(2021 - LocalDate.ofEpochDay(randomDay).getYear()),
                randomPlaats
        );
    }

    private static String generateString(int maxWordLength, int wordCount) {
        final String KLINKERS = "aeoui";
        final String MEDEKLINKERS = "qwrtypsdfghjklzxcvbnm";
        StringBuilder wordBuilder = new StringBuilder();
        StringBuilder nameBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < wordCount; i++) {
            for (int j = random.nextInt(maxWordLength / 2); j < maxWordLength; j++) {
                if (random.nextInt(3) + 1 > 2) {
                    char randomChar = MEDEKLINKERS.charAt(random.nextInt(MEDEKLINKERS.length()));
                    if(wordBuilder.length() == 0){
                        wordBuilder.append(Character.toUpperCase(randomChar));
                    }
                    wordBuilder.append(randomChar);
                } else {
                    char randomChar = KLINKERS.charAt(random.nextInt(KLINKERS.length()));
                    if(wordBuilder.length() == 0){
                        wordBuilder.append(Character.toUpperCase(randomChar));
                    }
                    wordBuilder.append(randomChar);
                }
            }
            nameBuilder.append(wordBuilder);
            nameBuilder.append(" ");
            wordBuilder.setLength(0);
        }
        return nameBuilder.toString();

    }
}
