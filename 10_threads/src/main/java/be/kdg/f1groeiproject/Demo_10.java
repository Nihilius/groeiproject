package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.Hoofdkwartier;
import be.kdg.f1groeiproject.threading.F1TeamRunnable;

import javax.swing.*;
import java.time.LocalDate;
import java.util.List;

public class Demo_10 {
    public static void main(String[] args) {
        final int TESTCOUNT = 100;
        double avgTime = 0;
        for (int i = 0; i < TESTCOUNT; i++) {

            F1TeamRunnable kampioenschappen = new F1TeamRunnable(f1team -> f1team.getKampioenschappen() > 4);
            F1TeamRunnable nieuweTeams = new F1TeamRunnable(f1Team -> f1Team.getEersteDeelname().isAfter(LocalDate.of(1990, 1, 1)));
            F1TeamRunnable hoofdkwartieren = new F1TeamRunnable(f1Team -> f1Team.getHoofdkwartier().equals(Hoofdkwartier.ITALY));

            Thread threadKampioenschappen = new Thread(kampioenschappen, "Kampioenschappen");
            Thread threadNieuweTeams = new Thread(nieuweTeams, "Nieuwe teams");
            Thread threadHoofdkwartier = new Thread(hoofdkwartieren, "Hoofdkwartier");

            try {
                threadKampioenschappen.start();
                threadKampioenschappen.join();
                kampioenschappen.getF1Teams().stream().limit(5).forEach(f1Team -> System.out.println(f1Team.toString()));
                System.out.println("=====================================================================================\n");
                threadNieuweTeams.start();
                threadNieuweTeams.join();
                nieuweTeams.getF1Teams().stream().limit(5).forEach(f1Team -> System.out.println(f1Team.toString()));
                System.out.println("=====================================================================================\n");
                threadHoofdkwartier.start();
                threadHoofdkwartier.join();
                hoofdkwartieren.getF1Teams().stream().limit(5).forEach(f1Team -> System.out.println(f1Team.toString()));
                System.out.println("=====================================================================================\n");
                avgTime += System.currentTimeMillis();

            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("3 threads verzamelen elk 1000 F1Teams (Gemiddlde uit 100 runs): " + (avgTime / TESTCOUNT));
    }
}
