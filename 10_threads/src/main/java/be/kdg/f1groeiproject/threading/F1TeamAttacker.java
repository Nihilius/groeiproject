package be.kdg.f1groeiproject.threading;

import be.kdg.f1groeiproject.model.F1Team;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class F1TeamAttacker implements Runnable {
    List<F1Team> attackList;
    Predicate<F1Team> predicate;

    public F1TeamAttacker(List<F1Team> attackList, Predicate<F1Team> predicate) {
        this.attackList = attackList;
        this.predicate = predicate;
    }

    @Override
    public void run() {
        synchronized (this) {
            attackList.removeIf(predicate);
        }
    }
}
