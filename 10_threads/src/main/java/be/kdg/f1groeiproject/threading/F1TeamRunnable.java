package be.kdg.f1groeiproject.threading;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1TeamFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class F1TeamRunnable implements Runnable {
    private Predicate<F1Team> predicate;
    private List<F1Team> f1Teams;

    public F1TeamRunnable(Predicate<F1Team> predicate){
        this.predicate = predicate;
        this.f1Teams = new ArrayList<>();
    }

    @Override
    public void run() {
        f1Teams = Stream.generate((Supplier<F1Team>) F1TeamFactory::newRandomF1Team).limit(1000).
                filter(predicate).
                collect(Collectors.toList());
    }

    public List<F1Team> getF1Teams() {
        return f1Teams;
    }
}
