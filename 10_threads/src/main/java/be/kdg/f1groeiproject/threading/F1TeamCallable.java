package be.kdg.f1groeiproject.threading;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1TeamFactory;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class F1TeamCallable implements Callable<List<F1Team>> {
    Predicate<F1Team> predicate;

    public F1TeamCallable(Predicate<F1Team> predicate){
        this.predicate = predicate;
    }

    @Override
    public List<F1Team> call() throws Exception {
        List<F1Team> f1TeamList = Stream.generate(F1TeamFactory::newRandomF1Team).limit(1000).collect(Collectors.toList());
        f1TeamList.removeIf(predicate);
        return f1TeamList;
    }
}
