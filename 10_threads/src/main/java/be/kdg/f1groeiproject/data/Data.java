package be.kdg.f1groeiproject.data;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.Hoofdkwartier;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Data {
    private static ArrayList<F1Team> f1Teams = new ArrayList<>();


    public Data() {
        f1Teams.add(new F1Team("Scuderia Ferrari", "Michael Schumacher", 0.23939,  LocalDate.of(1950,5,21), 16, Hoofdkwartier.ITALY));
        f1Teams.add(new F1Team("Alfa Romeo Racing", "Juan Manuel Fangio", 0.07633,  LocalDate.of(1950,5,13), 0, Hoofdkwartier.ITALY));
        f1Teams.add(new F1Team("Scuderia Alpha Tauri", "Sebastian Vettel", 0.00373,  LocalDate.of(2006,3,12), 0, Hoofdkwartier.ITALY));
        f1Teams.add(new F1Team("Haas F1 Team", "Kevin Magnussen", 0.0,  LocalDate.of(2016,3,20), 0, Hoofdkwartier.US));
        f1Teams.add(new F1Team("Mclaren F1 Team", "Ayrton Senna", 0.21089,  LocalDate.of(1966,5,22), 8, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Racing Point F1 Team", "Sergio Perez", 0.0,  LocalDate.of(2019,3,17), 0, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Mercedes-AMG Formula One Team", "Lewis Hamilton", 0.48571,  LocalDate.of(1954,6,4), 6, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Red Bull Racing", "Sebastian Vettel", 0.21678,  LocalDate.of(2005,3,6), 4, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Renault F1 Team", "Fernando Alonso", 0.09138,  LocalDate.of(1977,7,16), 2, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Williams Racing", "Nigel Mansell", 0.15681,  LocalDate.of(1977,5,8), 9, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Team Lotus", "Jim Clark", 0.15133,  LocalDate.of(1958,5,18), 7, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Tyrrell Racing Organisation", "Jackie Stewart", 0.07127,  LocalDate.of(1968,1,1), 1, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Brabham", "Nelson Piquet", 0.08883,  LocalDate.of(1962,8,5), 2, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("British Racing Motors", "Graham Hill", 0.08629,  LocalDate.of(1951,7,14), 1, Hoofdkwartier.UK));
        f1Teams.add(new F1Team("Cooper Car Company", "Jack Brabham", 0.12403,  LocalDate.of(1950,5,21), 2, Hoofdkwartier.UK));
    }


    public static List<F1Team> getData(){
        return f1Teams;
    }
}
