package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.threading.F1TeamAttacker;
import be.kdg.f1groeiproject.threading.F1TeamCallable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Demo_12 {
    public static void main(String[] args) {
        final int TESTCOUNT = 100;
        long avgTime = 0;
        for (int i = 0; i < TESTCOUNT; i++) {

            List<List<F1Team>> futureList = new ArrayList<>();
            F1TeamCallable callDatum = new F1TeamCallable(f1Team -> f1Team.getEersteDeelname().isAfter(LocalDate.of(2001, 1, 1)));
            F1TeamCallable callKampioenschap = new F1TeamCallable(f1Team -> f1Team.getKampioenschappen() > 10);
            F1TeamCallable callWinpercent = new F1TeamCallable(f1Team -> f1Team.getWinPercent() < 0.5);

            ExecutorService exServ = Executors.newFixedThreadPool(3);

            try {
                Future<List<F1Team>> futureDatum = exServ.submit(callDatum);
                futureList.add(futureDatum.get());
                System.out.println(System.currentTimeMillis());
                Future<List<F1Team>> futureKampioenschap = exServ.submit(callKampioenschap);
                futureList.add(futureKampioenschap.get());
                System.out.println(System.currentTimeMillis());
                Future<List<F1Team>> futureWin = exServ.submit(callWinpercent);
                futureList.add(futureWin.get());
                System.out.println(System.currentTimeMillis());
                avgTime += System.currentTimeMillis();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            } finally {
                exServ.shutdown();
            }
        }

        System.out.println("3 Futures verzamelen elk 1000 dictators (Gemiddelde uit 100 runs): " + (avgTime/TESTCOUNT));
    }
}
