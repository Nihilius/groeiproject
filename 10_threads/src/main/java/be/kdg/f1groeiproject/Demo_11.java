package be.kdg.f1groeiproject;

import be.kdg.f1groeiproject.model.F1Team;
import be.kdg.f1groeiproject.model.F1TeamFactory;
import be.kdg.f1groeiproject.threading.F1TeamAttacker;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_11 {
    public static void main(String[] args) {
        List<F1Team> f1TeamList = Stream.generate(F1TeamFactory::newRandomF1Team).limit(1000).collect(Collectors.toList());


        F1TeamAttacker attackDatum = new F1TeamAttacker(f1TeamList, f1Team -> f1Team.getEersteDeelname().isAfter(LocalDate.of(2001, 1, 1)));
        F1TeamAttacker attackKampioenschappen = new F1TeamAttacker(f1TeamList, f1Team -> f1Team.getKampioenschappen() > 10);
        F1TeamAttacker attackWinPercent = new F1TeamAttacker(f1TeamList, f1Team -> f1Team.getWinPercent() < 0.5);

        Thread threadDatum = new Thread(attackDatum, "Datum");
        Thread threadKampioenschappen = new Thread(attackKampioenschappen, "Kampioenschappen");
        Thread threadWinPercent = new Thread(attackWinPercent, "Winpercentage");


        try {
            System.out.println("Na zuivering:");
            threadDatum.start();
            threadDatum.join();
            System.out.println("Aantal teams opgestart in 21e eeuw: " + f1TeamList.stream().filter(f1Team -> f1Team.getEersteDeelname().isAfter(LocalDate.of(2001, 1, 1))).collect(Collectors.toList()).size());
            threadKampioenschappen.start();
            threadKampioenschappen.join();
            System.out.println("Aantal teams met minder dan 10 kampioenschappen: " + f1TeamList.stream().filter(f1Team -> f1Team.getKampioenschappen() > 10).collect(Collectors.toList()).size());
            threadWinPercent.start();
            threadWinPercent.join();
            System.out.println("Aantal teams met winpercentage van minstens 50%: " + f1TeamList.stream().filter(f1Team -> f1Team.getWinPercent() < 0.5).collect(Collectors.toList()).size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}